colord-gtk (0.3.1-1) unstable; urgency=medium

  * New upstream release
  * Release to unstable (Closes: #1062095)
  * Build-depend on dpkg-dev (>= 1.22.5)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sat, 02 Mar 2024 18:12:21 -0500

colord-gtk (0.3.0-4.1~exp1) experimental; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.

 -- Steve Langasek <vorlon@debian.org>  Wed, 31 Jan 2024 11:01:08 +0000

colord-gtk (0.3.0-4) unstable; urgency=medium

  * Team upload

  [ Jeremy Bícha ]
  * Add debian/upstream/metadata

  [ Debian Janitor ]
  * Re-export upstream signing key without extra signatures
  * Remove constraints unnecessary since buster (oldstable)
  * Update standards version to 4.6.2, no changes needed

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 15 Jun 2023 12:04:11 -0400

colord-gtk (0.3.0-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix typo "Replacs" in debian/control. Closes: #1034921

 -- Andreas Metzler <ametzler@debian.org>  Thu, 18 May 2023 14:17:42 +0200

colord-gtk (0.3.0-3) unstable; urgency=medium

  * Source-only rebuild

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 31 Mar 2022 14:12:29 -0400

colord-gtk (0.3.0-2) unstable; urgency=medium

  * Team upload
  * Build gtk4 library (Closes: #1008678)
  * Add libcolord-gtk-headers package for shared headers for both
    GTK3 & GTK4
  * Add libcolord-gtk-doc package
  * debian/libcolord-gtk1.install: Stop installing useless /usr/share/locale/
  * Bump Standards-Version to 4.6.0

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 30 Mar 2022 16:56:42 -0400

colord-gtk (0.3.0-1) unstable; urgency=medium

  * Team upload

  [ Jeremy Bicha ]
  * New upstream release
  * Build with meson
  * Bump debhelper-compat to 13
  * Build-Depend on dh-sequence-gir
  * Add debian/docs to install NEWS

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Tue, 22 Feb 2022 08:56:44 -0500

colord-gtk (0.1.26-2) unstable; urgency=medium

  [ Christopher James Halse Rogers ]
  * Acknowledge NMU. Thanks to Andreas Moog!
  * Drop the GTK2 build, it's unused (Closes: #883334)
  * Add GIR dependency on the -dev package, as per gobject-introspection
    mini-policy.
  * debian/copyright:
    + Use https protocol for copyright-format URL
    + Use https:// for upstream URL
  * debian/control:
    + Wrap long line in colord-gtk-utils extended description
    + colord-gtk-utils can inherit Section from the source package
    + Use https:// URI for Homepage
    + Use canonical Vcs-* URIs.
    + Mark libcolord-gtk-dev and gir1.2- packages as Multi-Arch: same
  * debian/rules:
    + Correctly set hardening flags
  * debian/gir1.2-colordgtk-1.0.install: Install typelib into multiarch path.
  * debian/watch: Add PGP signature verification
  * Bump Standards-Version to 4.1.2

  [ Jeremy Bicha ]
  * Bump debhelper compat to 10

 -- Christopher James Halse Rogers <raof@ubuntu.com>  Tue, 12 Dec 2017 11:54:22 +1100

colord-gtk (0.1.26-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add intltool to Build-Depends to fix FTBFS with newer version of
    gtk-doc-tools (Closes: #837843)

 -- Andreas Moog <andreas.moog@warperbbs.de>  Thu, 08 Dec 2016 18:07:34 +0100

colord-gtk (0.1.26-1) unstable; urgency=medium

  * Acknowledge NMU. Thanks to Margarita Manterola!
  * Package new cd-convert utility in colord-gtk-utils
  * Add a GTK2 build of colord-gtk library.
    Somewhat late, but there's still plenty of GTK2 code around that
    could benefit. (Closes: 678715)
  * Bump Standards-Version; no changes required
  * Add docbook-utils Build-Dependency for cd-convert manpage generation.

 -- Christopher James Halse Rogers <raof@ubuntu.com>  Fri, 27 Nov 2015 07:56:07 +0100

colord-gtk (0.1.25-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add Breaks-Replaces with gir1.2-colord-1.0 << 0.1.31-1, which used to ship
    /usr/lib/girepository-1.0/ColordGtk-1.0.typelib. (Closes: 718002)

 -- Margarita Manterola <marga@debian.org>  Sun, 24 Nov 2013 19:45:01 +0100

colord-gtk (0.1.25-1) unstable; urgency=low

  [ Jeremy Bicha ]
  * New upstream release
  * debian/control:
    - Bump minimum colord
    - Build with default valac

  [ Christopher James Halse Rogers ]
  * debian/copyright:
    - Update licence for cd-sample-widget.*

 -- Christopher James Halse Rogers <raof@ubuntu.com>  Thu, 27 Jun 2013 11:54:55 +1000

colord-gtk (0.1.24-1) experimental; urgency=low

  * Initial release (Closes: 702620)
  * Based on the Ubuntu packaging; thanks Jeremy Bicha and Robert Ancell!

 -- Christopher James Halse Rogers <raof@ubuntu.com>  Sat, 09 Mar 2013 18:11:02 +1100
